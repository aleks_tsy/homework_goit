import sys
from pathlib import Path

JPEG_IMAGES = []
JPG_IMAGES = []
PNG_IMAGES = []
SVG_IMAGES = []
MP3_AUDIO = []
ZIP_ARCHIVES = []
AVI_VIDEO = []
MP4_VIDEO = []
MOV_VIDEO = []
MKV_VIDEO = []
DOC_DOCUMENT = []
DOCX_DOCUMENT = []
TXT_DOCUMENT = []
PDF_DOCUMENT = []
XLSX_DOCUMENT = []
PPTX_DOCUMENT = []

OGG_AUDIO = []
WAV_AUDIO = []
AMR_AUDIO = []
MY_OTHER = []

GZ_ARCHIVES = []
TAR_ARCHIVES = []

REGISTER_EXTENSIONS = {
    'JPEG': JPEG_IMAGES,
    'PNG': PNG_IMAGES,
    'JPG': JPG_IMAGES,
    'SVG': SVG_IMAGES,
    'MP3': MP3_AUDIO,
    'ZIP': ZIP_ARCHIVES,
    'AVI': AVI_VIDEO,
    'MP4':MP4_VIDEO,
    'MOV':MOV_VIDEO,
    'MKV':MKV_VIDEO,
    'DOC':DOC_DOCUMENT,
    'DOCX':DOCX_DOCUMENT,
    'TXT':TXT_DOCUMENT,
    'PDF':PDF_DOCUMENT,
    'XLSX':XLSX_DOCUMENT,
    'PPTX':PPTX_DOCUMENT,
    'OGG':OGG_AUDIO,
    'WAV':WAV_AUDIO,
    'AMR':AMR_AUDIO,
    'GZ':GZ_ARCHIVES,
    'TAR':TAR_ARCHIVES,
}

FOLDERS = []
EXTENSIONS = set()
UNKNOWN = set()



def get_extension(filename: str) -> str:
    # перетворюємо розширення файлу на назву папки .jpg -> JPG
    return Path(filename).suffix[1:].upper()



def scan(folder: Path) -> None:
    for item in folder.iterdir():
        # Якщо це папка то додаємо її зі списку FOLDERS і переходимо до наступного елемента папки
        if item.is_dir():
            # перевіряємо, щоб папка не була тією, в яку ми складаємо вже файли.
            if item.name not in ('archives', 'video', 'audio', 'documents', 'images', 'MY_OTHER'):
                FOLDERS.append(item)
                # скануємо цю вкладену папку - рекурсія
                scan(item)
            # перейти до наступного елемента в сканованій папці
            continue

        # Робота з файлом
        ext = get_extension(item.name)  # взяти розширення
        fullname = folder / item.name  # взяти повний шлях до файлу
        if not ext:  # якщо файл не має розширення додати до невідомих
            MY_OTHER.append(fullname)
        else:
            try:
                # взяти список куди покласти повний шлях до файлу
                container = REGISTER_EXTENSIONS[ext]
                EXTENSIONS.add(ext)
                container.append(fullname)
            except KeyError:
                # Якщо ми не реєстрували розширення у REGISTER_EXTENSIONS, то додати до іншого
                UNKNOWN.add(ext)
                MY_OTHER.append(fullname)



if __name__ == '__main__':


    folder_for_scan = sys.argv[1]
    print(f'Start in folder {folder_for_scan}')


    scan(Path(folder_for_scan))
    print(f'Images jpeg: {JPEG_IMAGES}')
    print(f'Images jpg: {JPG_IMAGES}')
    print(f'Images svg: {SVG_IMAGES}')
    print(f'Audio mp3: {MP3_AUDIO}')
    print(f'Archives zip: {ZIP_ARCHIVES}')
    print(f'Video avi:{AVI_VIDEO}')
    print(f'Video mp4:{MP4_VIDEO}')
    print(f'Video mov:{MOV_VIDEO}')
    print(f'Video mkv:{MKV_VIDEO}')
    print(f'Document doc:{DOC_DOCUMENT}')
    print(f'Document docx:{DOCX_DOCUMENT}')
    print(f'Document txt:{TXT_DOCUMENT}')
    print(f'Document pdf:{PDF_DOCUMENT}')
    print(f'Document xlsx:{XLSX_DOCUMENT}')
    print(f'Document pptx:{PPTX_DOCUMENT}')
    print(f'Document mp3:{MP3_AUDIO}')
    print(f'Audio ogg:{OGG_AUDIO}')
    print(f'Audio wav:{WAV_AUDIO}')
    print(f'Audio amr:{AMR_AUDIO}')
    print(f'Archives gz:{GZ_ARCHIVES}')
    print(f'Archives tar:{TAR_ARCHIVES}')


    print(f'Types of files in folder: {EXTENSIONS}')
    print(f'Unknown files of types: {UNKNOWN}')


    print(FOLDERS[::-1])


# запускаємо:  python3 main.py `назва_папки_для_сортування`